Git commands:


1. To create git repository for our folder, run command on the terminal:

$ git init

- git init stands for git initialized, it will create a hidden '.git' folder inside of our folder
- this '.git' contains all the necessary3 files and folders to run the git repository and track the changes of our folder.
- git init is executed only once

2. To check the status of the untracked files of our folder, run the command:

$ git status

3. Then to add the changes or the revisions on the staging area, run the command:

$ git add -A

- -A (flag), stands for 'all'
- git add . vs git add -A => There's no difference at all

3. To save/commit our history to our git repository, run command:

$ git commit -m "Initial Commit"

- git commit, saves the history on our git repository
- -m (flag), stands for 'message', it is required everytime we commit our changes
- -m flag must always contain a message written enclosed in a double quote
- messages is used to help us developers to identify what are the changes that we have made on our commits/history
- we add a message "Initial Commit" since its our FIRST time to save our version to our git repository

4. If your git repository has returned a fatal error with "*** Please tell me who you are.", this only means that the git repository 
does not know us yet, so we wanna make sure that we set up our accounts identity on our git repository, to do that run the command below:

$ git config --global user.name "Your Name" 

	ex: git config --global user.name "Christine Llapitan"
  
$ git config --global user.email "Your Email"

	ex: git config --global user.email "christine.llapitan@tuitt.com"
  
- we do this to tell our git repository who we are
- to check if our user name and user email are set up properly we can run the command below:

$ git config --global user.name

- this will return the set up name for git repository

$ git config --global user.email

- this will return the set up email for the git repository
- to change the set up user name and email, we can run the command below:

$ git config --global user.name "Your NEW name"

- and

$ git config --global user.email "Your NEW email"


